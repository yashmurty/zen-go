package engine

import (
	"fmt"

	"bitbucket.org/yashmurty/zen-go/domain"
)

func (engine *arenaEngine) SaveKnight(knight *domain.Knight) {
	engine.knightRepository.Save(knight)
}

func (engine *arenaEngine) GetKnight(ID string) (*domain.Knight, error) {
	fighter := engine.knightRepository.Find(ID)
	if fighter == nil {
		return nil, fmt.Errorf("Knight #%s not found.", ID)
	}

	return fighter, nil
}

func (engine *arenaEngine) ListKnights() []*domain.Knight {
	return engine.knightRepository.FindAll()
}

func (engine *arenaEngine) Fight(fighter1ID string, fighter2ID string) domain.Fighter {
	return nil
}
