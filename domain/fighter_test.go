package domain

import "testing"

func TestKnight(t *testing.T) {
	knight1 := &Knight{"ID1", "John", 20, 10}

	if knight1.GetID() != "ID1" {
		t.Error("could not get ID")
	}

	if knight1.GetPower() != 30 {
		t.Error("could not get power")
	}
}
