package domain

// Fighter interface is to describe what can fight.
type Fighter interface {
	GetID() string
	GetPower() float64
}

// Knight is a struct. It's an entity that can be stored in the database.
type Knight struct {
	ID          string `json:"id" gorm:"primary_key"`
	Name        string `json:"name" gorm:"type:varchar(255);null" binding:"omitempty"`
	Strength    int    `json:"strength" gorm:"type:int;not null" binding:"required"`
	WeaponPower int    `json:"weapon_power" gorm:"type:int;not null" binding:"required"`
}

// GetID returns a String which represents the ID of the combatant.
func (k *Knight) GetID() string {
	return k.ID
}

// GetPower returns a Float which represents the Power level of the Fighter.
func (k *Knight) GetPower() float64 {
	return float64(k.Strength + k.WeaponPower)
}
