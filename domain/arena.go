package domain

import (
	"math/big"
)

// Arena ...
type Arena struct{}

// Fight takes 2 fighters and made them fight by comparing their Power Level.
func (arena *Arena) Fight(fighter1 Fighter, fighter2 Fighter) Fighter {
	// NOTE: Floating point numbers will end up imprecise due to rounding errors.
	// Use math/big to compare the float values.

	// convert float to type math/big.Float
	var power1 = big.NewFloat(fighter1.GetPower())
	var power2 = big.NewFloat(fighter2.GetPower())

	// compare power1 to power2
	result := power1.Cmp(power2)

	// -1 if power1 < power2
	if result < 0 {
		return fighter2
	}

	// +1 if power1 > power2
	if result > 0 {
		return fighter1
	}

	// Result is 0 if power1 == power2. Return nil for draw.
	return nil
}
