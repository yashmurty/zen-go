# Notes

*Database setup, structure change, tests setup, etc.*    
- 1) Start the dependent servers with `docker-compose up`.    
- 2) Run the tests with `make test`.    
- 3) Run the API server with `make run`. (Run `make initdb` if running for the first time).    
- 4) To build the docker image run `make docker-build`.    