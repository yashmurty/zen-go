package database

import (
	"github.com/google/uuid"
	"github.com/jinzhu/gorm"

	"bitbucket.org/yashmurty/zen-go/domain"
)

type knightRepository struct {
	db *gorm.DB
}

// CreateTable creates a new table using name and schema.
func (repository *knightRepository) CreateTable(delete bool) error {
	if delete {
		if err := repository.db.DropTableIfExists(&domain.Knight{}).Error; err != nil {
			return err
		}
	}
	return repository.db.CreateTable(&domain.Knight{}).Error
}

func (repository *knightRepository) Find(ID string) *domain.Knight {
	var k domain.Knight

	if err := repository.db.Where("id = ?", ID).First(&k).Error; err != nil {
		return nil
	}
	return &k
}

func (repository *knightRepository) FindAll() []*domain.Knight {
	var k []*domain.Knight
	repository.db.Find(&k)
	return k
}

func (repository *knightRepository) Save(knight *domain.Knight) {
	if knight.ID == "" {
		knight.ID = uuid.New().String()
	}
	repository.db.Create(knight)
}
