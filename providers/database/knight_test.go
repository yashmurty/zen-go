package database

import (
	"testing"

	"github.com/google/uuid"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"

	"bitbucket.org/yashmurty/zen-go/domain"
)

func setupStore() knightRepository {
	db, err := gorm.Open("mysql", "user:password@tcp(localhost:3306)/zenport?parseTime=true")
	if err != nil {
		panic(err)
	}
	db.AutoMigrate(&domain.Knight{})

	store := knightRepository{db: db}
	// Empty the table.
	err = store.CreateTable(true)
	if err != nil {
		panic("failed to create db tables")
	}

	return store
}

func TestFind(t *testing.T) {
	store := setupStore()
	defer store.db.Close()

	t.Run("should successfully get an existing resource by its ID", func(t *testing.T) {
		resource := &domain.Knight{
			Strength:    10,
			WeaponPower: 20,
		}
		store.Save(resource)

		getResource := store.Find(resource.ID)

		if resource.ID != getResource.ID {
			t.Fatal("resource id does not match")
		}
		if resource.Strength != getResource.Strength {
			t.Fatal("resource strength does not match")
		}
	})

	t.Run("should return error for trying to get a nonexistent resource", func(t *testing.T) {
		getResource := store.Find("doesnotexist")
		if getResource != nil {
			t.Fatal("resource should not be found")
		}
	})
}

func TestFindAll(t *testing.T) {
	store := setupStore()
	defer store.db.Close()

	t.Run("should successfully get all resources", func(t *testing.T) {
		resource1 := &domain.Knight{
			Strength:    10,
			WeaponPower: 20,
		}
		store.Save(resource1)

		resource2 := &domain.Knight{
			Strength:    30,
			WeaponPower: 40,
		}
		store.Save(resource2)

		getResources := store.FindAll()

		if len(getResources) != 2 {
			t.Fatal("all resources could not be found")
		}
	})

}

func TestSave(t *testing.T) {
	store := setupStore()
	defer store.db.Close()

	t.Run("should successfully create a resource given required fields", func(t *testing.T) {
		id := uuid.New().String()
		resource := &domain.Knight{
			ID:       id,
			Strength: 10,
		}
		store.Save(resource)

		if resource.ID == "" {
			t.Fatal("resource id is empty")
		}
		if resource.Strength != 10 {
			t.Fatal("resource strength does not match")
		}
	})

	t.Run("should successfully create a resource with empty id", func(t *testing.T) {
		resource := &domain.Knight{
			Strength: 15,
		}
		store.Save(resource)

		if resource.ID == "" {
			t.Fatal("resource id is empty")
		}
		if resource.Strength != 15 {
			t.Fatal("resource strength does not match")
		}
	})
}
