package database

import (
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"

	"bitbucket.org/yashmurty/zen-go/engine"
)

// Provider ...
type Provider struct {
	db *gorm.DB
}

func NewProvider() *Provider {
	db, err := gorm.Open("mysql", "user:password@tcp(localhost:3306)/zenport?parseTime=true")
	if err != nil {
		panic(err)
	}
	return &Provider{db}
}

// GetKnightRepository manipulates and stores a knight.
func (provider *Provider) GetKnightRepository() engine.KnightRepository {
	return &knightRepository{provider.db}
}

func (provider *Provider) Close() {
	provider.db.Close()
}
