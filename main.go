package main

import (
	"flag"
	"os"
	"os/signal"
	"syscall"

	"bitbucket.org/yashmurty/zen-go/adapters/http"
	"bitbucket.org/yashmurty/zen-go/engine"
	"bitbucket.org/yashmurty/zen-go/providers/database"
)

var (
	InitDB bool
)

func main() {
	flag.BoolVar(&InitDB, "initdb", false, "Drop the database and recreate all tables")
	flag.Parse()

	provider := database.NewProvider()
	// Check if we should (re)initialize DB.
	if InitDB {
		err := initDB(provider)
		if err != nil {
			panic(err)
		} else {
			println("It's a Done Deal.")
			os.Exit(0)
		}
	}

	e := engine.NewEngine(provider)

	adapter := http.NewHTTPAdapter(e)
	adapter.SetupHTTPHandlers()

	stop := make(chan os.Signal, 1)
	signal.Notify(stop, syscall.SIGINT, syscall.SIGTERM)
	defer close(stop)

	adapter.Start()

	<-stop

	adapter.Stop()
	provider.Close()
}

// initDB deletes all existing tables and recreates them.
func initDB(provider *database.Provider) error {
	err := provider.GetKnightRepository().CreateTable(true)
	if err != nil {
		return err
	}

	return nil
}
