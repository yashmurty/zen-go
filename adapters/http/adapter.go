package http

import (
	"context"
	"net/http"
	"time"

	"bitbucket.org/yashmurty/zen-go/domain"
	"bitbucket.org/yashmurty/zen-go/engine"
	"bitbucket.org/yashmurty/zen-go/pkg/logger"
	"github.com/gin-gonic/gin"
	"go.uber.org/zap"
)

type HTTPAdapter struct {
	GinEngine *gin.Engine
	Addr      string
	Engine    engine.Engine
}

var (
	log = logger.Get("adapter")
	srv = &http.Server{}
)

// Start the HTTP server.
func (adapter *HTTPAdapter) Start() {
	// todo: start to listen
	srv.Addr = adapter.Addr
	srv.Handler = adapter.GinEngine

	go func() {
		// service connections
		if err := srv.ListenAndServe(); err != nil && err != http.ErrServerClosed {
			log.Fatal("failed to start HTTP server", zap.Error(err))
		}
	}()

}

// Stop the HTTP server gracefully.
func (adapter *HTTPAdapter) Stop() {
	// todo: shutdown server
	log.Info("Shutdown Server ...")

	timeout := 1
	ctx, cancel := context.WithTimeout(context.Background(), time.Duration(timeout)*time.Second)
	defer cancel()
	if err := srv.Shutdown(ctx); err != nil {
		log.Fatal("Server Shutdown:", zap.Error(err))
	}
	// catching ctx.Done(). timeout of 5 seconds.
	select {
	case <-ctx.Done():
		log.Info("Successfully timed out.", zap.Int("timeout", timeout))
	}
	log.Info("Server exiting")
}

// NewHTTPAdapter creates a new API server
func NewHTTPAdapter(e engine.Engine) *HTTPAdapter {
	// todo: init your http server and routes
	router := gin.Default()
	router.GET("/", func(c *gin.Context) {
		c.String(http.StatusOK, "OK")
	})

	return &HTTPAdapter{
		Addr:      ":8080",
		GinEngine: router,
		Engine:    e,
	}
}

// SetupHTTPHandlers sets up http handlers on the given api server.
func (adapter *HTTPAdapter) SetupHTTPHandlers() {
	adapter.GinEngine.Handle("POST", "/knight", adapter.PostKnightHandler)
	adapter.GinEngine.Handle("GET", "/knight", adapter.GetKnightsHandler)
	adapter.GinEngine.Handle("GET", "/knight/:id", adapter.GetKnightHandler)

}

// PostKnightHandler will create a knight.
func (adapter *HTTPAdapter) PostKnightHandler(c *gin.Context) {
	r := &domain.Knight{}
	err := c.BindJSON(r)
	if err != nil {
		APIError(c, http.StatusBadRequest, err)
		return
	}

	knight := &domain.Knight{
		Name:        r.Name,
		Strength:    r.Strength,
		WeaponPower: r.WeaponPower,
	}

	adapter.Engine.SaveKnight(knight)

	c.JSON(http.StatusCreated, knight)
}

// GetKnightsHandler will Get list of knights.
func (adapter *HTTPAdapter) GetKnightsHandler(c *gin.Context) {
	knights := adapter.Engine.ListKnights()
	c.JSON(200, knights)
}

// GetKnightHandler will Get one knight.
func (adapter *HTTPAdapter) GetKnightHandler(c *gin.Context) {
	id := c.Param("id")

	knight, err := adapter.Engine.GetKnight(id)
	if err != nil {
		APIError(c, http.StatusNotFound, err)
		return
	}

	c.JSON(200, knight)
}

// APIError returns a JSON error with useful metadata.
func APIError(c *gin.Context, code int, err error) {
	if code >= http.StatusInternalServerError {
		log.Error(
			"api error",
			zap.String("path", c.Request.RequestURI),
			zap.Error(err),
		)
	}
	c.AbortWithStatusJSON(code, NewAPIStatus(code, err.Error()))
}

// APIStatus represents a standard JSON response when calling
// endpoints.
type APIStatus struct {
	Code    int    `json:"code"`
	Message string `json:"message"`
}

// NewAPIStatus creates a new APIStatus.
func NewAPIStatus(code int, message string) APIStatus {
	return APIStatus{
		Code:    code,
		Message: message,
	}
}
