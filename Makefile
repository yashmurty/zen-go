#!make
.PHONY: test initdb run docker-build docker-run

test:
	# domain package
	go test bitbucket.org/yashmurty/zen-go/domain -cover -count=1
	# database package
	go test bitbucket.org/yashmurty/zen-go/providers/database -cover -count=1
	# http package
	go test bitbucket.org/yashmurty/zen-go/adapters/http -cover -count=1

initdb:
	go run main.go --initdb

run:
	go run main.go

docker-build:
	docker build . -t go-assignment

docker-run:
	docker run -p 3030:3030 go-assignment