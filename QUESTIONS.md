# Questions

For this assignment you also have to answer a couple of questions.
There is no correct answer and none is mandatory, if you don't know just skip it.

 - **What do you think of the initial project structure ?**
It's great. I really appreciate this assignment becasue it already had a project structure. 
I wasted very less time in thinking about the project structure myself, which can generally be
a very time consuming exercise. 


 - **What you will improve from your solution ?**
If I had more time I would do the following things:
    - Use `go mod` for dependency management.
    - Change the signatures of my functions in `database/providers` to return errors.

 - **For you, what are the boundaries of a service inside a micro-service architecture ?**  
This is a definitely a very subjective question. For me personally, I try to define the boundaries around business objects and their relations. One of the ways that I try to do is to write down 
user stories for the features and see what kind of resources/objects are repeating and which of them are part of the same story multiple times. This generally gives me a good starting point to define the service boundaries.
In the end, I see the micro-service architecture as something that solves an organizational challenge, rather than a technical one.


 - **For you, what are the most relevant usage for SQL, NoSQL, key-value and document store ?**
I would like to answer this question in detail later. 
Unfortunately I'm out of time and would like to submit my assignment first. :)

